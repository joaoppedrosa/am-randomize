package com.jppedrosa.amrandomize

import android.app.Application

/**
 * @author João Pedro Pedrosa (<a href="mailto:joaopopedrosa@gmail.com">joaopopedrosa@gmail.com</a>) on 08/09/2022.
 */
class AMRandomizeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
    }
}