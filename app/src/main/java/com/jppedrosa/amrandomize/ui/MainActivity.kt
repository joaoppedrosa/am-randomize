package com.jppedrosa.amrandomize.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jppedrosa.amrandomize.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}